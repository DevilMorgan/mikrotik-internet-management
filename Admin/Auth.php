<?php
require '../ConnectToService.php';

if (isset($_POST['Admin_Username']) && isset($_POST['Admin_Password']) &&  !empty($_POST['Admin_Username']) && !empty($_POST['Admin_Password'])) {
    $Admin_Username       =    mysqli_real_escape_string($conn, $_POST['Admin_Username']);
    $Admin_Password       =    mysqli_real_escape_string($conn, $_POST['Admin_Password']);


    $QueryAdminInfomation = $conn->query("SELECT * FROM `Administrator_info` WHERE `AdminUsername` LIKE '$Admin_Username' ");
    if ($QueryAdminInfomation->num_rows > 0) {
        session_start();
        $AdminData = $QueryAdminInfomation->fetch_array();
        if (password_verify($Admin_Password, $AdminData['AdminPassword'])) {
            $_SESSION['user_id'] =  $AdminData['AdminUsername'];
            $Admin_Username      =  $AdminData['AdminName'];
            $AlertType = "LoginSucces";
        } else {
            $AlertType = "EmptyInput";
        }
    } else {
        $AlertType = "EmptyInput";
    }
} else {
    $AlertType = "EmptyInput";
}
$ReturnInfomation = array($AlertType, $Admin_Username);

print json_encode($ReturnInfomation);
