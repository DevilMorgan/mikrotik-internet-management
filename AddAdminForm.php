<?php
session_start();

$debugingcode = false;

if (isset($_SESSION['user_id'])) {
    $isLoggedin = true;
} else {
    $isLoggedin = false;
}

  if ($isLoggedin == true) {
  } elseif ($debugingcode === false) {
      header("location:./Admin/");
  }

$captcha;
if (isset($_POST['g-recaptcha-response'])) {
    $captcha=$_POST['g-recaptcha-response'];
    $response=json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6Lf-jokUAAAAAKkCEsAsdYe0_Bbjg10_fXS2848L&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']), true);
}
?>

<!DOCTYPE html>
<html lang="en">
   <head>
      <!--  meta tags-->
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <!-- Title Page-->
      <title>เพิ่มผู้ดูแลระบบ</title>
      <!-- Icons font CSS-->
      <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
      <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
      <!-- Font special for pages-->
      <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
      <!-- Vendor CSS-->
      <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
      <link href="vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all">
      <!-- Main CSS-->
      <link href="css/main.css" rel="stylesheet" media="all">
      <script src='https://www.google.com/recaptcha/api.js?hl=th'></script>
      <script type="text/javascript" src="js/sweetalert.min.js"></script>
   </head>
   <body>
      <div class="page-wrapper bg-gra-02 p-t-130 p-b-100">
         <div class="wrapper wrapper--w680">
            <div class="card card-4">
               <div class="card-body">
                 <center>
                  <h2 class="title">เพิ่มผู้ดูแลระบบ</h2>
                </center>
                  <form method="post" action="./AddAdmin.php" action="post" id="RegisterInternetIDForm" name="RegisterInterNet">
                     <div class="row row-space">
                        <div class="col-2">
                           <div class="input-group">
                              <label class="label">กรุณาเลือก Username(ชื่อผู้ใช้)</label>
                              <div class="input-group-icon">
                                 <input class="input--style-4" type="text" name="AdminUsername" required>
                                 <i class="fa fa-user input-icon"></i>
                              </div>
                           </div>
                        </div>
                        <div class="col-2">
                           <div class="input-group">
                              <label class="label">กรุณาเลือก Password(รหัสผ่าน)</label>
                              <div class="input-group-icon">
                                 <input class="input--style-4" type="password" name="AdminPassword" required>
                                 <i class="fa fa-user input-icon"></i>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row row-space">
                        <div class="col-2">
                          <div class="input-group">
                             <label class="label">กรุณาใส่ชื่อผู้ดูแล</label>
                             <div class="input-group-icon">
                                <input class="input--style-4" type="text" name="AdminName" required>
                                <i class="fa fa-address-card input-icon"></i>
                             </div>
                          </div>
                        </div>
                        <div class="col-2">
                           <div class="input-group">
                              <label class="label">อีเมล / Email</label>
                              <div class="input-group-icon">
                              <input class="input--style-4" type="email" name="AdminEmail" required>
                              <i class="fa fa-envelope input-icon"></i>
                           </div>
                           </div>
                        </div>
                    </div>
                    <center>
                    <div class="g-recaptcha" data-callback="UnlockButtonRecap" data-sitekey="6Lf-jokUAAAAAKMqIs_GnTC8qNmuIblMSbsyze8F"></div>
                    <div class="p-t-15">
                       <button id="GoogleRecapButton" class="btn btn--radius-2 btn--blue" type="submit" disabled>ยืนยัน</button>
                       <button class="btn btn--radius-2 btn--blue" onclick="window.location.href='Admin/index.php'">ย้อนกลับ</button>
                     </center>
                    </div>
                    </div>
                    </div>
                    </div>
                    </div>
                    </div>
                    </form>
               </div>
            </div>
         </div>
      </div>
      <!-- Jquery JS-->
      <script src="vendor/jquery/jquery.min.js"></script>
      <!-- Vendor JS-->
      <script src="vendor/select2/select2.min.js"></script>
      <script src="vendor/datepicker/moment.min.js"></script>
      <script src="vendor/datepicker/daterangepicker.js"></script>
      <!-- Main JS-->
      <script src="js/global.js"></script>
   </body>
</html>
<script>
function UnlockButtonRecap(){
      document.getElementById('GoogleRecapButton').disabled = false;
}
</script>
