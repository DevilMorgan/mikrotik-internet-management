-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 10, 2020 at 02:49 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `databasemim`
--

-- --------------------------------------------------------

--
-- Table structure for table `administrator_info`
--

CREATE TABLE `administrator_info` (
  `AdminUsername` varchar(50) CHARACTER SET utf8 NOT NULL,
  `AdminPassword` varchar(255) CHARACTER SET utf8 NOT NULL,
  `AdminName` varchar(50) CHARACTER SET utf8 NOT NULL,
  `Email` varchar(50) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `administrator_info`
--

INSERT INTO `administrator_info` (`AdminUsername`, `AdminPassword`, `AdminName`, `Email`) VALUES
('admin', '$2y$10$WRCBnIQN9PSp5fMi/E.KR.3NA1a.E1oXxsQy2E9FpyQ8cd02oio6y', 'Admin', 'admin@admin.com');

-- --------------------------------------------------------

--
-- Table structure for table `departmentmanager`
--

CREATE TABLE `departmentmanager` (
  `Department_ID` varchar(50) CHARACTER SET utf8 NOT NULL,
  `Department_Name` varchar(50) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `departmentmanager`
--

INSERT INTO `departmentmanager` (`Department_ID`, `Department_Name`) VALUES
('01', 'ยานยนต์'),
('02', 'ก่อสร้าง'),
('03', 'เทคนิคพื้นฐาน'),
('04', 'คอมพิวเตอร์ธุรกิจ'),
('05', 'ไฟฟ้ากำลัง'),
('06', 'อิเล็กทรอนิกส์'),
('07', 'การตลาด'),
('08', 'การบัญชี'),
('09', 'สถานศึกษานอกวิทยาลัย');

-- --------------------------------------------------------

--
-- Table structure for table `gradechecking`
--

CREATE TABLE `gradechecking` (
  `Student_ID` int(50) NOT NULL,
  `FirstName` varchar(50) CHARACTER SET utf8 NOT NULL,
  `LastName` varchar(50) CHARACTER SET utf8 NOT NULL,
  `Grade_Thai` float NOT NULL DEFAULT 0,
  `Grade_Math` float NOT NULL DEFAULT 0,
  `Grade_Scient` float NOT NULL DEFAULT 0,
  `Grade_Social` float NOT NULL DEFAULT 0,
  `Grade_English` float NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `userinformation`
--

CREATE TABLE `userinformation` (
  `name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `lastname` varchar(50) CHARACTER SET utf8 NOT NULL,
  `ID_Passport` varchar(15) CHARACTER SET utf8 NOT NULL,
  `Email` varchar(50) CHARACTER SET utf8 NOT NULL,
  `BirthDate` int(3) NOT NULL,
  `BirthMonth` varchar(3) CHARACTER SET utf8 NOT NULL,
  `BirthYears` int(5) NOT NULL,
  `Course` int(2) NOT NULL,
  `StudentYear` int(2) NOT NULL,
  `ClassNumber` int(2) NOT NULL,
  `Department` int(2) NOT NULL,
  `PhoneNumber` varchar(11) NOT NULL,
  `IsVerified` tinyint(1) NOT NULL,
  `GeneratedUser` varchar(10) CHARACTER SET utf8 NOT NULL,
  `GeneratedPass` varchar(8) CHARACTER SET utf8 NOT NULL,
  `JoinYear` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userinformation`
--

INSERT INTO `userinformation` (`name`, `lastname`, `ID_Passport`, `Email`, `BirthDate`, `BirthMonth`, `BirthYears`, `Course`, `StudentYear`, `ClassNumber`, `Department`, `PhoneNumber`, `IsVerified`, `GeneratedUser`, `GeneratedPass`, `JoinYear`) VALUES
('สมศักดิ์', 'ดีมากมี', '1321654646463', 'somsak@company.com', 1, '01', 2543, 1, 1, 1, 4, '0123456789', 0, '6010401463', '01012543', 2560);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `administrator_info`
--
ALTER TABLE `administrator_info`
  ADD PRIMARY KEY (`AdminUsername`);

--
-- Indexes for table `departmentmanager`
--
ALTER TABLE `departmentmanager`
  ADD PRIMARY KEY (`Department_ID`);

--
-- Indexes for table `gradechecking`
--
ALTER TABLE `gradechecking`
  ADD PRIMARY KEY (`Student_ID`);

--
-- Indexes for table `userinformation`
--
ALTER TABLE `userinformation`
  ADD PRIMARY KEY (`ID_Passport`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
